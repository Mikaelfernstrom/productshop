/**
 * 
 */

/**
 * @author Mikael
 *
 */
public class Golfbag extends Product {

	private String m�rke;
	private String typ;
	
	public String getM�rke() {
		return m�rke;
	}
	public void setM�rke(String m�rke) {
		this.m�rke = m�rke;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String toString() {
		return "M�rke: " + getM�rke() + "\nTyp: " + getTyp() + super.toString();
	}
}
