/**
 * 
 */

/**
 * @author Mikael
 *
 */
public class Order {

	private int id;
	private Product vara;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Product getVara() {
		return vara;
	}
	public void setVara(Product vara) {
		this.vara = vara;
	}
	
}
