/**
 * 
 */

/**
 * @author Mikael
 *
 */
public class Cykel extends Fordon {

	private int antalPedaler; // Antal pedaler p� cykel
	private String modell;
	
	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	public int getAntalPedaler() {
		return antalPedaler;
	}

	public void setAntalPedaler(int antalPedaler) {
		this.antalPedaler = antalPedaler;
	}

	// Printar ut informationen om cykeln
	public void printDescription() {
		System.out.println("Cykeln har " + getAntalPedaler() + " pedaler."
				+ ".");
	}
	public String toString() {
		return "Modell: " + getModell() + "\nAntal pedaler: " + getAntalPedaler() + super.toString();
	}
}
