/**
 * 
 */

/**
 * @author Mikael
 *
 */
public class Kund {

	private String name;
	private Product köptProduct;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Product getKöptProduct() {
		return köptProduct;
	}
	public void setKöptProduct(Product köptProduct) {
		this.köptProduct = köptProduct;
	}
	public void printDescription() {
		System.out.println(getName()+"\n\n"+"Order:\n----------------\n"+ köptProduct+"\n"+"----------------");
	}
}
