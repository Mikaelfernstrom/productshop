/**
 * 
 */

/**
 * @author Mikael
 *
 */
import java.util.Scanner;




public class Meny {
	
	static Scanner input = new Scanner(System.in);
	
	
	public void MainMenu(){
		
		System.out.println("V�lkommen till Lindh & F�rnstr�ms!");
		System.out.println("Vad vill du k�pa?\n1. Sportbil\n2. Lastbil\n3. Cykel\n4. Golfbag");
		Sportbil sportbil1 = new Sportbil();
		Lastbil lastbil1 = new Lastbil();
		Cykel cykel1 = new Cykel();
		Golfbag golfbag1 = new Golfbag();
		Order order1 = new Order();
		Butik butik1 = new Butik();
		
		Kund kund1 = new Kund();
		 int userIn = input.nextInt();
        
         do {
                 switch (userIn) {
                 case 1:                                      	                
                	
         			sportbil1.setNamn("Ferrari F-50");
         			sportbil1.setHastighet(400);
         			sportbil1.setAntalV�xlar(6);
         			sportbil1.setHjul(4);
         			sportbil1.setD�rrar(2);
         			sportbil1.setLagerSaldo(5);
         			sportbil1.setProdNr(1);
         			sportbil1.setPris(2000000);
         			
         			
        			order1.setId(1);
        			order1.setVara(sportbil1);
        			        			        			        			        			        			
        			Product levereradSportbil = butik1.s�ljaProduct(order1);
        			
        			kund1.setName("Viktor Lindh");
        			kund1.setK�ptProduct(levereradSportbil);
        			kund1.printDescription();
        			
        			
                         break;
                        
                 case 2:
                	
         			lastbil1.setNamn("Volvo");
         			lastbil1.setLastkapacitet(6000);
         			lastbil1.setAntalV�xlar(8);
         			lastbil1.setHjul(6);
         			lastbil1.setD�rrar(2);
         			lastbil1.setLagerSaldo(2);
         			lastbil1.setPris(2000000000);
         			lastbil1.setProdNr(6);
         			
         			
         			order1.setId(1);
        			order1.setVara(lastbil1);
        			        			        			        			        			        			
        			Product levereradLastbil = butik1.s�ljaProduct(order1);
        			
        			kund1.setName("Viktor Lindh");
        			kund1.setK�ptProduct(levereradLastbil);
        			kund1.printDescription();
                         break;
                        
                 case 3:
                	
        			cykel1.setModell("MTB");
        			cykel1.setAntalPedaler(2);
         			cykel1.setAntalV�xlar(24);
         			cykel1.setHjul(2);
         			cykel1.setLagerSaldo(5);
         			cykel1.setPris(7999);
         			cykel1.setProdNr(12);
         			
         			order1.setId(1);
        			order1.setVara(cykel1);
        			        			        			        			        			        			
        			Product levereradCykel = butik1.s�ljaProduct(order1);
        			
        			kund1.setName("Viktor Lindh");
        			kund1.setK�ptProduct(levereradCykel);
        			kund1.printDescription();
         			
                 
                	 	 break;	
                 case 4:
                	
         			golfbag1.setM�rke("Callaway");
         			golfbag1.setTyp("B�rbag");
         			golfbag1.setProdNr(3);
         			golfbag1.setPris(1999);
         			golfbag1.setLagerSaldo(14);
         			
         			order1.setId(1);
        			order1.setVara(golfbag1);
        			        			        			        			        			        			
        			Product levereradGolfbag = butik1.s�ljaProduct(order1);
        			
        			kund1.setName("Viktor Lindh");
        			kund1.setK�ptProduct(levereradGolfbag);
        			kund1.printDescription();
         			
         			
                	 	 break;
                	 	
                 case 5:
                	 
                	 	System.exit(0);
                        
                 default:
                         System.out.println("Sorry, that�s not a correct choice! Try again:\n");
                         
                         break;

                 }
         }while (userIn >= 6);

 

		
	}
	

}