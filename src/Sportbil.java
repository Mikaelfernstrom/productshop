public class Sportbil extends Fordon implements Interface {
	private int hastighet;
	private String namn;
	
	public String getNamn() {
		return namn;
	}


	public void setNamn(String namn) {
		this.namn = namn;
	}


	public void setHastighet(int hastighet) {
		this.hastighet = hastighet;
	}


	public int getHastighet() {
		return hastighet;
	}
	
	public void bensin(){
		System.out.println("Jag g�r p� bensin! 5 liter / milen");
	}
	
	public String toString(){
		
		return "Modell: "+this.namn+ "\n"+"Top fart: " + this.hastighet + " KM/h \n"+super.toString();
	}
}