/**
 * 
 */

/**
 * @author Mikael
 *
 */
	public abstract class Product {
	
	private int prodNr;
	private double pris;
	private int lagerSaldo;
	
	public int getProdNr() {
		return prodNr;
	}
	public void setProdNr(int prodNr) {
		this.prodNr = prodNr;
	}
	public double getPris() {
		return pris;
	}
	public void setPris(double pris) {
		this.pris = pris;
	}
	public int getLagerSaldo() {
		return lagerSaldo;
	}
	public void setLagerSaldo(int lagerSaldo) {
		this.lagerSaldo = lagerSaldo;
	}
	
	public String toString(){
		return "\nProdukt nummer: " + getProdNr() + "\nPris: " + getPris() + "\nLagersaldo: "+ getLagerSaldo() + "";
	}

	
}
